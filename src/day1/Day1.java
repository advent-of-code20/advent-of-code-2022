package day1;

import static handy.Handy.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;;

public class Day1 {
	public static int part1(String input) {
		return Pattern.compile(NEWLINE + NEWLINE)
				.splitAsStream(input)
				.map(s -> Pattern.compile(NEWLINE).splitAsStream(s))
				.mapToInt(s -> s.mapToInt(Integer::parseInt).sum())
				.max().orElse(0);
	}
	
	public static int part2(String input) {
		return Pattern.compile(NEWLINE + NEWLINE)
				.splitAsStream(input)
				.map(s -> Pattern.compile(NEWLINE).splitAsStream(s))
				.mapToInt(s -> s.mapToInt(Integer::parseInt).sum())
				.boxed()
				.sorted(Comparator.reverseOrder())
				.limit(3)
				.mapToInt(Integer::valueOf)
				.sum();
	}
	
	public static void main(String... args) {
		int result1 = part1(loadString("src/day1/input.txt"));
		int result2 = part2(loadString("src/day1/input.txt"));
		System.out.println(result1);
		System.out.println(result2);
	}
}
