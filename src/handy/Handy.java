package handy;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Provides handy file stuff!
 * 
 * @author Thomas Whaley (TXW303)
 *
 */
public class Handy {
	public static final String NEWLINE = "\r\n";
	
	/**
	 * Loads all lines from a file.
	 * 
	 * @param filename Filename
	 * @return List of lines
	 */
	public static List<String> loadLines(String filename) {
		try {
			return Files.readAllLines(Paths.get(filename));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	
	/**
	 * Loads all words from a file.
	 * 
	 * @param filename Filename
	 * @return List of words
	 */
	public static List<String> loadStrings(String filename) {
		try {
			return List.of(Files.readString(Paths.get(filename)).split("\\w+"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return List.of();
	}
	
	/**
	 * Loads all integers from a file.
	 * 
	 * @param filename Filename
	 * @return List of lines
	 */
	public static List<Integer> loadLinesInt(String filename) {
		return loadStrings(filename).stream().filter(s -> s.matches("\\d+")).mapToInt(Integer::parseInt).boxed().toList();
	}
	
	/**
	 * Loads all doubles from a file.
	 * 
	 * @param filename Filename
	 * @return List of doubles
	 */
	public static List<Double> loadLinesDouble(String filename) {
		return loadStrings(filename).stream().filter(s -> s.matches("[\\d.]+")).mapToDouble(Double::parseDouble).boxed().toList();
	}

	/**
	 * Loads the entire contents of the file into a string.
	 * 
	 * @param filename Filename
	 * @return File contents
	 */
	public static String loadString(String filename) {
		try {
			return Files.readString(Paths.get(filename));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
